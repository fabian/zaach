rtfm:
	cat Makefile

init:
	# Install and upgrade requirements - if any
	if [ $$(stat -L -f "%z" requirements.txt) -ne 0 ]; then \
	    pip install --upgrade -r requirements.txt; \
	fi

test:
	python2 -m unittest discover tests 'test*.py' '.'
	python3 -m unittest discover tests 'test*.py' '.'

twine:
	pip install twine

release: test twine
	python3 setup.py sdist
	twine upload dist/*

releasetest: test twine
	python3 setup.py sdist
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

.PHONY: rtfm init test twine release releasetest
